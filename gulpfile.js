const gulp = require('gulp');
var sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

gulp.task('sass', () => {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('dist/css'))
});

gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });

  gulp.watch(['./*.html', './dist/css/**/*.css']).on('change', browserSync.reload);
});

gulp.task('watch', () => {
  gulp.watch(['./src/scss/**/*.scss'], gulp.parallel(['sass']));
});

gulp.task('default', gulp.parallel(['sass', 'watch', 'browser-sync']));
